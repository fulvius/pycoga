import pygame
import numpy as np

from utility import HelpBox, InputBox

RED = (255,0,0)
ORANGE = (255,153,0)
GREEN = (0,255,0)
WHITE = (255,255,255)
BLACK = (0,0,0)
BLUE = (0,0,255)

###############################################################################

class Manipulator():
    """docstring for Manipulator."""
    def __init__(self):

        self.n = 2
        self.q = np.zeros((self.n,1))
        self.l1 = 100
        self.l2 = 100
        self.lw = 4
        self.origin = np.zeros((self.n,1))
        self.arm_1 = np.zeros((self.n,1))
        self.arm_2 = np.zeros((self.n,1))
        self.arm_color = ORANGE
        self.target_color = RED
        self.ee = self.arm_2
        self.target = np.zeros((self.n,1))
        self.target_radius = 6
        self.points = 0
        self.q1_pressed = np.zeros((self.n,1))
        self.q2_pressed = np.zeros((self.n,1))

    def step(self, q):

        self.arm_1[0,0] = self.origin[0,0] + self.l1 * np.cos(q[0,0])
        self.arm_1[1,0] = self.origin[1,0] + self.l1 * np.sin(q[0,0])

        self.arm_2[0,0] = self.arm_1[0,0] + self.l2 * np.cos(q[0,0] + q[1,0])
        self.arm_2[1,0] = self.arm_1[1,0] + self.l2 * np.sin(q[0,0] + q[1,0])

        self.ee = self.arm_2

    def num2pix(self, x):
        x_p = np.round(x)
        return x_p.astype(int)

    def draw(self, win):

        arm_1 =  self.num2pix(self.arm_1)
        arm_2 =  self.num2pix(self.arm_2)

        pygame.draw.line(win, self.arm_color, self.origin, arm_1, self.lw)
        pygame.draw.line(win, self.arm_color, arm_1, arm_2, self.lw)

        target = self.num2pix(self.target)
        pygame.draw.circle(win, self.target_color, target, self.target_radius)

    def sampleTarget(self):

        alpha = 2 * np.pi * np.random.rand()
        radius = np.sqrt(np.random.rand())
        self.target[0,0] = (self.l1+self.l2) * radius * np.cos(alpha) + self.origin[0,0]
        self.target[1,0] = (self.l1+self.l2) * radius * np.sin(alpha) + self.origin[1,0]

    def reachTarget(self, win):

        if (np.linalg.norm(self.ee - self.target) < 10):
            self.points += 1
            self.sampleTarget()

    def inputControl(self, event):

        keys = pygame.key.get_pressed()

        if (event.type == pygame.KEYDOWN):
            if (event.key == pygame.K_a):
                self.q1_pressed[0] = 1
            if (event.key == pygame.K_s):
                self.q1_pressed[1] = 1
            if (event.key == pygame.K_x):
                self.q2_pressed[0] = 1
            if (event.key == pygame.K_z):
                self.q2_pressed[1] = 1
        elif (event.type == pygame.KEYUP):
            if (event.key == pygame.K_a):
                self.q1_pressed[0] = 0
            if (event.key == pygame.K_s):
                self.q1_pressed[1] = 0
            if (event.key == pygame.K_x):
                self.q2_pressed[0] = 0
            if (event.key == pygame.K_z):
                self.q2_pressed[1] = 0

    def setJoints(self):

        if (self.q1_pressed[0] == 1):
            self.q[0,0] += np.deg2rad(1)
        elif (self.q1_pressed[1] == 1):
            self.q[0,0] -= np.deg2rad(1)
        elif (self.q1_pressed[0] == 0):
            self.q[0,0] = self.q[0,0]
        elif (self.q1_pressed[1] == 0):
            self.q[0,0] = self.q[0,0]

        self.q[0,0] = np.clip(self.q[0,0], -np.pi, np.pi)

        if (self.q2_pressed[0] == 1):
            self.q[1,0] += np.deg2rad(1)
        elif (self.q2_pressed[1] == 1):
            self.q[1,0] -= np.deg2rad(1)
        elif (self.q2_pressed[0] == 0):
            self.q[1,0] = self.q[1,0]
        elif (self.q2_pressed[1] == 0):
            self.q[1,0] = self.q[1,0]

        self.q[1,0] = np.clip(self.q[1,0], -np.pi, np.pi)


###############################################################################

def displayPoints(win, font, points):

    text = 'Points: %d' % points
    text_surface = font.render(text, True, WHITE)
    win.blit(text_surface, (100, 50))

###############################################################################

def displayTime(win, font):

    text = 'Time: %.2f' % (pygame.time.get_ticks()/1000)
    text_surface = font.render(text, True, WHITE)
    win.blit(text_surface, (300, 50))

###############################################################################


def main():

    screen_size = (600, 700)
    q = np.zeros((2,1))

    pygame.init()
    win = pygame.display.set_mode(screen_size)
    pygame.display.set_caption('Reach target')
    font = pygame.font.Font('freesansbold.ttf', 20)

    delay = 5
    delay_box = InputBox(x=screen_size[0]/4, y=650, w=50, h=40, text=str(delay), text_descr='Pygame delay', bc=BLUE)  # n

    text_help = "Controll the robot to reach the target!\n\nFor controlling the robot use:\n- (A,S) for the first joint\n" \
                "- (Z,X) for the second joint\n\nYou can adjust the pygame delay.\n\n\nClick 'Return' to continue"
    help_box = HelpBox(x=screen_size[0]*2/3, y=650, w=50, h=40, screen_size=(450,350), bc=BLUE, text_descr=text_help, original_caption='Reach target')


    bot = Manipulator()
    bot.origin[0,0] = screen_size[0] - bot.l1*(bot.n+1)
    bot.origin[1,0] = screen_size[1]/2
    bot.sampleTarget()

    width = bot.l1*(bot.n + 1)*1.5
    height = width
    sim_box = pygame.Rect(bot.origin[0] - width/2, bot.origin[1] - height/2, width, height)

    run = True

    while (run):

        pygame.time.delay(delay)

        win.fill(BLACK)

        displayPoints(win, font, bot.points)
        displayTime(win, font)

        pygame.draw.rect(win, WHITE, sim_box, 2)

        delay_box.draw(win)
        help_box.drawBox(win)

        bot.step(bot.q)
        bot.reachTarget(win)
        bot.draw(win)

        for event in pygame.event.get():

            if (event.type == pygame.QUIT):
                run = False

            bot.inputControl(event)

            delay_box.getEvent(event)
            delay = delay_box.value

            help_box.getEvent(event, win)

        bot.setJoints()

        pygame.display.update()

    pygame.quit()


if __name__ == '__main__':
    main()
