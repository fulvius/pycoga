import pygame
import numpy as np

from utility import HelpBox, InputBox

RED = (255,0,0)
ORANGE = (255,153,0)
GREEN = (0,255,0)
WHITE = (255,255,255)
BLACK = (0,0,0)
BLUE = (0,0,255)

###############################################################################

class Manipulator():
    """docstring for Manipulator."""
    def __init__(self):

        self.n = 2
        self.q = np.zeros((self.n+1,1))
        self.l1 = 100
        self.l2 = 100
        self.l3 = 10

        self.lw = 4
        self.origin = np.zeros((self.n,1))
        self.arm_1 = np.zeros((self.n,1))
        self.arm_2 = np.zeros((self.n,1))
        self.arm_3 = np.zeros((self.n,1))
        self.arm_color = ORANGE
        self.object_color = RED
        self.goal_color = GREEN
        self.ee = self.arm_2
        self.object = np.zeros((self.n,1))
        self.goal = np.zeros((self.n,1))
        self.object_radius = 10
        self.goal_radius = 10
        self.points = 0
        self.object = self.sampleTarget()
        self.goal = self.sampleTarget()
        self.q1_pressed = [0,0]
        self.q2_pressed = [0,0]

    def step(self, q):

        self.arm_1[0,0] = self.origin[0,0] + self.l1 * np.cos(q[0,0])
        self.arm_1[1,0] = self.origin[1,0] + self.l1 * np.sin(q[0,0])

        self.arm_2[0,0] = self.arm_1[0,0] + self.l2 * np.cos(q[0,0] + q[1,0])
        self.arm_2[1,0] = self.arm_1[1,0] + self.l2 * np.sin(q[0,0] + q[1,0])

        self.arm_3[0,0] = self.arm_2[0,0] + self.l3 * np.cos(q[0,0] + q[1,0])
        self.arm_3[1,0] = self.arm_2[1,0] + self.l3 * np.sin(q[0,0] + q[1,0])


    def num2pix(self, x):
        x_p = np.round(x)
        return x_p.astype(int)

    def draw(self, win):

        arm_1 = self.num2pix(self.arm_1)
        arm_2 = self.num2pix(self.arm_2)
        arm_3 = self.num2pix(self.arm_3)
        pygame.draw.line(win, self.arm_color, self.origin, arm_1, self.lw)
        pygame.draw.line(win, self.arm_color, arm_1, arm_2, self.lw)
        pygame.draw.circle(win, self.arm_color, arm_3, self.l3)

        object = self.num2pix(self.object)
        pygame.draw.circle(win, self.object_color, object, self.object_radius)
        goal = self.num2pix(self.goal)
        pygame.draw.circle(win, self.goal_color, goal, self.goal_radius)

    def sampleTarget(self):

        alpha = 2 * np.pi * np.random.rand()
        radius = np.sqrt(np.random.rand())
        object = np.zeros((2,1))
        object[0,0] = (self.l1+self.l2) * radius * np.cos(alpha) + self.origin[0,0]
        object[1,0] = (self.l1+self.l2) * radius * np.sin(alpha) + self.origin[1,0]
        return object

    def newScenario(self):
        self.object = self.sampleTarget()
        self.goal = self.sampleTarget()

    def reachTarget(self, win):

        if (np.linalg.norm(self.goal - self.object) < 10):
            self.points += 1
            self.newScenario()

    def collision(self, point):

        min_dist = self.lw + self.object_radius + 1
        object_ = self.num2pix(self.object)
        point = self.num2pix(point)
        dist = np.linalg.norm(object_ - point)

        if (dist < min_dist):
            angle = np.arctan2(- point[1,0] + object_[1,0], -point[0,0] + object_[0,0])
            overlap = 0 + min_dist - dist
            self.object[0,0] += np.cos(angle) * overlap
            self.object[1,0] += np.sin(angle) * overlap

    def checkCollision(self):

        obj_1 = np.linspace(self.origin, self.arm_1, self.l1)
        obj_2 = np.linspace(self.arm_1, self.arm_2, self.l2)
        theta = np.linspace(0,2*np.pi,360)
        obj_3 = []
        for t in theta:
            arm_3 = self.arm_3
            arm_3[0,0] += self.l3 * np.cos(t)
            arm_3[1,0] += self.l3 * np.sin(t)
            obj_3.append(arm_3)

        obj = np.vstack((obj_1,obj_2, obj_3))

        dist_list = []
        for point in obj:
            dist_list.append(np.linalg.norm(self.object - point))

        ind = np.argmin(dist_list)
        point = obj[ind]

        self.collision(point)

    def inputControl(self, event):

        keys = pygame.key.get_pressed()

        if (event.type == pygame.KEYDOWN):
            if (event.key == pygame.K_a):
                self.q1_pressed[0] = 1
            if (event.key == pygame.K_s):
                self.q1_pressed[1] = 1
            if (event.key == pygame.K_x):
                self.q2_pressed[0] = 1
            if (event.key == pygame.K_z):
                self.q2_pressed[1] = 1
        elif (event.type == pygame.KEYUP):
            if (event.key == pygame.K_a):
                self.q1_pressed[0] = 0
            if (event.key == pygame.K_s):
                self.q1_pressed[1] = 0
            if (event.key == pygame.K_x):
                self.q2_pressed[0] = 0
            if (event.key == pygame.K_z):
                self.q2_pressed[1] = 0

    def setJoints(self):

        if (self.q1_pressed[0] == 1):
            self.q[0,0] += np.deg2rad(1)
        elif (self.q1_pressed[1] == 1):
            self.q[0,0] -= np.deg2rad(1)
        elif (self.q1_pressed[0] == 0):
            self.q[0,0] = self.q[0,0]
        elif (self.q1_pressed[1] == 0):
            self.q[0,0] = self.q[0,0]

        self.q[0,0] = np.clip(self.q[0,0], -np.pi, np.pi)

        if (self.q2_pressed[0] == 1):
            self.q[1,0] += np.deg2rad(1)
        elif (self.q2_pressed[1] == 1):
            self.q[1,0] -= np.deg2rad(1)
        elif (self.q2_pressed[0] == 0):
            self.q[1,0] = self.q[1,0]
        elif (self.q2_pressed[1] == 0):
            self.q[1,0] = self.q[1,0]

        self.q[1,0] = np.clip(self.q[1,0], -np.pi, np.pi)


###############################################################################

def displayPoints(win, font, points):

    text = 'Points: %d' % points
    text_surface = font.render(text, True, WHITE)
    win.blit(text_surface, (100, 50))

###############################################################################

def displayTime(win, font):

    text = 'Time: %.2f' % (pygame.time.get_ticks()/1000)
    text_surface = font.render(text, True, WHITE)
    win.blit(text_surface, (300, 50))

###############################################################################

def main():

    screen_size = (600, 700)
    q = np.zeros((3,1))

    pygame.init()
    win = pygame.display.set_mode(screen_size)
    pygame.display.set_caption('Push target')
    font = pygame.font.Font('freesansbold.ttf', 20)

    delay = 5
    delay_box = InputBox(x=screen_size[0]/4, y=650, w=50, h=40, text=str(delay), text_descr='Pygame delay', bc=BLUE)

    text_help = "Controll the robot to push the target!\n\nFor controlling the robot use:\n- (A,S) for the first joint\n" \
                "- (Z,X) for the second joint\n\nYou can adjust the pygame delay.\n\n\nClick 'Return' to continue"
    help_box = HelpBox(x=screen_size[0]*2/3, y=650, w=50, h=40, screen_size=(450,350), bc=BLUE, text_descr=text_help, original_caption='Push target')

    bot = Manipulator()
    bot.origin[0,0] = screen_size[0] - bot.l1*(bot.n+1)
    bot.origin[1,0] = screen_size[1]/2
    bot.newScenario()

    width = bot.l1*(bot.n + 1)*2 - 100
    height = width
    sim_box = pygame.Rect(bot.origin[0] - width/2, bot.origin[1] - height/2, width, height)

    run = True

    while (run):

        pygame.time.delay(delay)

        win.fill(BLACK)

        displayPoints(win, font, bot.points)
        displayTime(win, font)

        pygame.draw.rect(win, WHITE, sim_box, 2)

        delay_box.draw(win)
        help_box.drawBox(win)

        bot.step(bot.q)

        bot.reachTarget(win)
        bot.draw(win)

        bot.checkCollision()

        for event in pygame.event.get():

            if (event.type == pygame.QUIT):
                run = False

            bot.inputControl(event)

            delay_box.getEvent(event)
            delay = delay_box.value

            help_box.getEvent(event, win)


        bot.setJoints()

        pygame.display.update()

    pygame.quit()


if __name__ == '__main__':
    main()
