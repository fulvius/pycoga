import pygame
import numpy as np
import matplotlib.pyplot as plt

###############################################################################

class CartPole():
    """docstring for CartPole."""
    def __init__(self):

        self.mp = 2
        self.mc = 20
        self.l = 4
        self.g = -9.81
        self.x = 0
        self.x_dot = 0
        self.x_ddot = 0
        self.theta = 0
        self.theta_dot = 0
        self.theta_ddot = 0
        self.f = 0
        self.h = 0.001 # dt

        self.width = 80
        self.height = 20
        self.box_color = (0,0,255)
        self.arm_color = (0,255,0)
        self.ball_color = (255,0,0)
        self.y = 200
        self.L = 100
        self.radius = 6

    def dxdt(self, x):
        return x

    def dthetadt(self, theta):
        return theta

    def dx_dotdt(self, theta, theta_dot):
        return (self.f + self.mp * np.sin(theta)*(self.l + theta_dot**2 + self.g * np.cos(theta))) / (self.mc + self.mp * np.sin(theta)**2)

    def dtheta_dotdt(self, theta, theta_dot):
        return (-self.f*np.cos(theta) - self.mp*self.l*(theta_dot**2)*np.cos(theta)*np.sin(theta) - (self.mp+self.mc)*self.g*theta) / (self.l*(self.mc + self.mp*np.sin(theta)**2))

    def euler(self):

        theta_ddot_n = self.dtheta_dotdt(self.theta, self.theta_dot)
        x_ddot_n = self.dx_dotdt(self.theta, self.theta_dot)

        theta_dot_n = theta_ddot_n * self.h + self.theta_dot
        x_dot_n = x_ddot_n * self.h + self.x_dot

        theta_n = theta_dot_n * self.h + self.theta
        x_n = x_dot_n * self.h + self.x

        self.x = x_n
        self.x_dot = x_dot_n
        self.theta = theta_n
        self.theta_dot = theta_dot_n

    def num2pix(self, x):
        x_p = np.round(x)
        return x_p.astype(int)

    def rotArm(self, theta):

        x = np.array([[self.L],[0]])
        R = np.array([[np.cos(theta), np.sin(theta)],[-np.sin(theta), np.cos(theta)]])
        x = R @ x
        x[0,0] += self.x
        x[1,0] += self.y - self.height/2
        return x

    def draw(self, win):
        x_px = self.num2pix(self.x)
        arm = self.rotArm(self.theta + np.pi/2)
        arm_px = self.num2pix(arm)

        pygame.draw.rect(win, self.box_color, (x_px-self.width/2, self.y-self.height/2, self.width, self.height))
        pygame.draw.line(win, self.arm_color, (x_px, self.y-self.height/2), (arm_px) )
        pygame.draw.circle(win, self.ball_color, (arm_px), self.radius, 0)

        # wheels
        pygame.draw.circle(win, (125,125,125), (x_px - int(self.width/4), self.y + 20), 10)
        pygame.draw.circle(win, (125,125,125), (x_px + int(self.width/4), self.y + 20), 10)

    def input(self, event):

            keys = pygame.key.get_pressed()

            if (keys[pygame.K_LEFT]):
                self.f -= 100000

            if (keys[pygame.K_RIGHT]):
                self.f += 100000

            if (keys[pygame.K_DOWN]):
                self.f = 0

###############################################################################

def lostGame(win, screen_size):

    text = 'Game Over'
    font = pygame.font.Font('freesansbold.ttf', 40)
    text_surface = font.render(text, True, (255,0,0))
    win.fill((0,0,0))
    win.blit(text_surface, (screen_size[0]/2-font.size(text)[0]/2, screen_size[1]/2-font.size(text)[1]/2))
    pygame.display.update()
    pygame.time.wait(1000)

###############################################################################

def initGame(win, screen_size):

    text = 'Survive as long as you can!'
    font = pygame.font.Font('freesansbold.ttf', 30)
    text_surface = font.render(text, True, (255,255,255))
    win.fill((0,0,0))
    win.blit(text_surface, (screen_size[0]/2-font.size(text)[0]/2, screen_size[1]/2-font.size(text)[1]/2))
    pygame.display.update()
    pygame.time.wait(1000)

    text = ['Use the arrow key to play', 'Left: pushing to left', 'Right: pushing to right', 'Down: set force to 0']
    font = pygame.font.Font('freesansbold.ttf', 26)
    win.fill((0,0,0))
    for i in range(len(text)):
        text_surface = font.render(text[i], True, (255,255,255))
        win.blit(text_surface, (screen_size[0]/2-font.size(text[i])[0]/2, screen_size[1]/2 - 100 + font.size(text[i])[1]*i*2 ))

    pygame.display.update()
    pygame.time.wait(5000)

###############################################################################

def main():

    screen_size = (800,300)
    delay = 50

    bot = CartPole()
    bot.x = screen_size[0]/2

    pygame.init()

    win = pygame.display.set_mode(screen_size)
    pygame.display.set_caption("CartPole")

    initGame(win, screen_size)


    run = True
    while (run):

        pygame.time.delay(delay)

        win.fill((0,0,0))

        bot.euler()

        bot.draw(win)


        for event in pygame.event.get():

            if (event.type == pygame.QUIT):
                run = False

            bot.input(event)

        if (bot.x > screen_size[0] or bot.x < 0):
            lostGame(win, screen_size)
            run = False

        pygame.display.update()

    pygame.quit()


if __name__ == '__main__':
    main()
