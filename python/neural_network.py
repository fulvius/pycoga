import pygame
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib.backends.backend_agg as agg

BLACK = (0,0,0)
BLUE = (0,0,255)
LIGHT_BLUE = (51, 204, 255)
GREEN = (0,255,0)
ORANGE = (255,153,0)
WHITE = (255,255,255)
GOLD = (255,215,0)
DARK_BLUE = (0, 0, 102)

n_neuros = 5
input_dim = 2
output_dim = 1
n_epochs = 500

W1 = np.zeros((n_epochs, input_dim, n_neuros))
b1 = np.zeros((n_epochs, n_neuros, ))
W2 = np.zeros((n_epochs, n_neuros, output_dim))
b2 = np.zeros((n_epochs, output_dim, ))

###############################################################################

class NeuralNetwork():
    """docstring for NeuralNetwork."""
    def __init__(self, input_dim, n_neuros, output_dim, W1, W2):
        super(NeuralNetwork, self).__init__()

        self.input_dim = input_dim
        self.n_neurons = n_neuros
        self.output_dim = output_dim

        self.i_x0 = 100
        self.i_y0 = 150
        self.n_x0 = self.i_x0 + 400
        self.n_y0 = 100
        self.n_y_gap = 150
        self.o_x0 = self.n_x0 + 400
        self.o_y0 = round(self.n_y0 + self.n_y_gap * (self.n_neurons - 1) / 2)
        self.radius = 40
        self.lw = 5
        self.lw2 = 5
        self.input_color = GREEN
        self.neuron_color = GOLD
        self.output_color = ORANGE
        self.epoch = 0
        self.W1 = W1
        self.W2 = W2

        self.P_I = None

        self.settings()

    def num2pix(self, x):

        x_p = np.round(x)
        return x_p.astype(int)

    def settings(self):
        p_i_1 = (self.i_x0, self.o_y0 - self.i_y0)
        p_i_2 = (self.i_x0, self.o_y0 + self.i_y0)
        self.P_I = [p_i_1, p_i_2]

    def mapping(self, x):

        a1 = -1
        a2 = 1
        b1 = 0
        b2 = 255
        color = (0, int(np.clip(round(b1 + (x - a1) * (b2 - b1) / (a2 - a1)), 0 ,255)), 255)

        b1 = 1
        b2 = 7
        lw =  int(round(b1 + (x - a1) * (b2 - b1) / (a2 - a1)))

        return color, lw


    def setEpoch(self):
        self.epoch += 1

    def draw(self, win):


        for p_i in self.P_I:
            pygame.draw.circle(win, self.input_color, p_i, self.radius, self.lw)



        for i in range(self.n_neurons):
            pygame.draw.circle(win, self.neuron_color, (self.n_x0, self.n_y0 + self.n_y_gap*i), self.radius, self.lw)

        pygame.draw.circle(win, self.output_color, (self.o_x0, self.o_y0), self.radius, self.lw)

        k = 0
        for p_i in self.P_I:

            for i in range(self.n_neurons):
                w1 = self.W1[self.epoch, k, i]
                color, lw = self.mapping(w1)

                p_n = (self.n_x0, self.n_y0 + self.n_y_gap*i)
                angle = np.arctan2(p_n[1] - p_i[1], p_n[0] - p_i[0])

                p_i_px = self.num2pix(  (p_i[0] + self.radius * np.cos(angle), p_i[1] + self.radius * np.sin(angle)) )
                p_n_px = self.num2pix(  (p_n[0] - self.radius * np.cos(angle), p_n[1] - self.radius * np.sin(angle)) )
                pygame.draw.line(win, color, p_i_px, p_n_px, lw)
            k += 1

        p_o = (self.o_x0, self.o_y0)
        k = 0
        for i in range(self.n_neurons):
            w2 = self.W2[self.epoch, k, 0]
            color, lw = self.mapping(w2)

            p_n = (self.n_x0, self.n_y0 + self.n_y_gap*i)
            angle = np.arctan2(p_o[1] - p_n[1], p_o[0] - p_n[0])

            p_o_px = self.num2pix(  (p_o[0] - self.radius * np.cos(angle), p_o[1] - self.radius * np.sin(angle)) )
            p_n_px = self.num2pix(  (p_n[0] + self.radius * np.cos(angle), p_n[1] + self.radius * np.sin(angle)) )
            pygame.draw.line(win, color, p_n_px, p_o_px, lw)
            k += 1

###############################################################################

class myCallBack(tf.keras.callbacks.Callback):

    def __init__(self):
        self.W1 = None
        self.b1 = None
        self.W2 = None
        self.b2 = None

    def on_epoch_end(self, epoch, logs=None):

        self.W1 = self.model.layers[0].get_weights()[0]
        self.b1 = self.model.layers[0].get_weights()[1]
        self.W2 = self.model.layers[1].get_weights()[0]
        self.b2 = self.model.layers[1].get_weights()[1]


        global W1, b1, W2, b2
        W1[epoch,:,:] = self.W1
        b1[epoch,:] = self.b1
        W2[epoch,:] = self.W2
        b2[epoch,:] = self.b2

###############################################################################

def trainNeuralNetwork():

    training_data = np.array([[0,0],[0,1],[1,0],[1,1]], "float32")
    target_data = np.array([[0],[1],[1],[0]], "float32")

    global n_neuros, input_dim, output_dim, n_epochs
    global W1, b1, W2, b2

    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Dense(n_neuros, input_dim=input_dim, activation='relu'))
    model.add(tf.keras.layers.Dense(output_dim, activation='sigmoid'))

    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['binary_accuracy'])

    history = model.fit(training_data, target_data, epochs=n_epochs, verbose=0, callbacks=[myCallBack()])
    loss_curve = history.history["loss"]
    acc_curve = history.history["binary_accuracy"]

    return loss_curve, acc_curve

###############################################################################

def blitText(win, text, init_pos, font):

    words = [word.split(' ') for word in text.splitlines()]
    space = font.size(' ')[0]
    max_width, max_height = win.get_size()
    x, y = init_pos
    for line in words:
        for word in line:
            word_surface = font.render(word, 0, WHITE)
            word_width, word_height = word_surface.get_size()
            if x + word_width >= max_width:
                x = init_pos[0]
                y += word_height
            win.blit(word_surface, (x,y))
            x += word_width + space
        x = init_pos[0]
        y += word_height

###############################################################################

def initialScreen():

    screen_size = (500, 220)
    win = pygame.display.set_mode(screen_size)
    pygame.display.set_caption("README")
    init_pos = (10,10)

    text = "Look what is going on when a neural network is learning!\n\nThe neural network is displayed on the left."\
            " Dark and thin links show weak connections, while light and big links show strong connections.\n" \
            "On the right the loss curve and the accurracy curve are displayed.\n\nClick 'Return' to continue."

    font = pygame.font.Font('freesansbold.ttf', 18)

    run = True
    while (run):

        blitText(win, text, init_pos, font)

        for event in pygame.event.get():

            keys = pygame.key.get_pressed()
            if (keys[pygame.K_RETURN]):
                run = False

        pygame.display.update()

###############################################################################

def plot(fig, ax, canvas, data_x, data_y1, data_y2):

   ax[0].clear()
   ax[1].clear()
   ax[0].plot(data_x, data_y1)
   ax[1].plot(data_x, data_y2)
   ax[0].set_xlabel('Epoch')
   ax[0].set_ylabel('Loss')
   ax[1].set_xlabel('Epoch')
   ax[1].set_ylabel('Accuracy')

   ax[0].set_title("Training")
   ax[0].grid()
   ax[1].grid()


   canvas.draw()
   renderer = canvas.get_renderer()

   raw_data = renderer.tostring_rgb()
   size = canvas.get_width_height()

   return pygame.image.fromstring(raw_data, size, "RGB")

###############################################################################

def main():

    # neural network training
    loss_curve, acc_curve = trainNeuralNetwork()

    screen_size = (1700, 850)
    delay = 50

    pygame.init()

    initialScreen()

    win = pygame.display.set_mode(screen_size)
    pygame.display.set_caption('Neural network')

    global n_neuros, input_dim, output_dim, n_epochs
    global W1, b1, W2, b2

    # neural network to draw
    nn = NeuralNetwork(input_dim, n_neuros, output_dim, W1, W2)

    # set figure
    fig, ax = plt.subplots(2,1,figsize=(6,8))
    fig_pos = (1000,20)
    canvas = agg.FigureCanvasAgg(fig)

    run = True
    while (run):

        pygame.time.delay(delay)

        win.fill(BLACK)

        nn.draw(win)
        nn.setEpoch()

        t_ = np.arange(0,nn.epoch,1)
        surf = plot(fig, ax, canvas, t_, loss_curve[0:nn.epoch], acc_curve[0:nn.epoch])
        win.blit(surf, fig_pos)

        for event in pygame.event.get():

            if (event.type == pygame.QUIT):
                run = False

        if (nn.epoch == n_epochs):
            run = False

        pygame.display.update()

    pygame.quit()

if __name__ == '__main__':
    main()
