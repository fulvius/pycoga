import pygame
import numpy as np
import control as cl
#
import matplotlib
# matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.backends.backend_agg as agg

from utility import InputBox, HelpBox, Slider

RED = (255, 0, 0)
ORANGE = (255, 153, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
DARK_BLUE = (0, 0, 100)
LIGHT_BLUE = (0, 153, 255)
GREY = (125, 125, 125)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

###############################################################################

def plot(fig, canvas, data_x, data_y):
   # ax.plot(data_x, data_y)
   plt.clf()
   plt.plot(data_x, data_y)
   plt.xlabel(r"$t$")
   plt.ylabel(r"$y$")
   plt.title("Step response")
   plt.grid()

   canvas.draw()
   renderer = canvas.get_renderer()

   raw_data = renderer.tostring_rgb()
   size = canvas.get_width_height()

   return pygame.image.fromstring(raw_data, size, "RGB")

###############################################################################

def main():

    fig = plt.figure(figsize=[6, 4])
    fig_pos = (50,20)
    # # ax = fig.add_subplot(111)
    canvas = agg.FigureCanvasAgg(fig)

    screen_size = (700, 800)
    delay = 100

    dt = 0.1
    T = 10
    t = np.arange(0,T,dt)

    pygame.init()

    win = pygame.display.set_mode(screen_size)
    pygame.display.set_caption("Pole placement")


    slider_sys_1 = Slider(x=125,y=550,w=100,h=10,ws=10,hs=50,val=-20,min_val=-100,max_val=100, text="z", fc=WHITE, ft=16)
    slider_sys_2 = Slider(x=125,y=650,w=100,h=10,ws=10,hs=50,val=-10,min_val=-100,max_val=100, text="p", fc=WHITE, ft=16)

    slider_cl_0 = Slider(x=475,y=550,w=100,h=10,ws=10,hs=50,val=1,min_val=-100,max_val=100, text="G0", fc=WHITE, ft=16)
    slider_cl_1 = Slider(x=475,y=650,w=100,h=10,ws=10,hs=50,val=-10,min_val=-100,max_val=100, text="z", fc=WHITE, ft=16)
    slider_cl_2 = Slider(x=475,y=750,w=100,h=10,ws=10,hs=50,val=0,min_val=-100,max_val=100, text="p", fc=WHITE, ft=16)

    sliders = [slider_sys_1, slider_sys_2, slider_cl_0, slider_cl_1, slider_cl_2]

    z_sys = slider_sys_1.val
    p_sys = slider_sys_2.val
    g0_cl = slider_cl_0.val
    z_cl = slider_cl_1.val
    p_cl = slider_cl_2.val
    G = cl.tf(np.poly([z_sys]),np.poly([p_sys]))
    C = g0_cl * cl.tf(np.poly([z_cl]),np.poly([p_cl]))
    G_cl = cl.feedback(C*G,1)

    text_sys = "System"
    text_cl = "Controller"

    font = pygame.font.Font('freesansbold.ttf', 18)
    text_surface_sys = font.render(text_sys, True, WHITE)
    text_surface_cl = font.render(text_cl, True, WHITE)

    text_help = "This is a pole placement simulation using pygame.\n\nThe plot is the system step response. The system is made of one zero and one pole,"\
                "while the controller is made of a gain, one zero, and one pole.\n\nYou can adjust the parameters with the sliders."\
                "Click 'Return' to continue."

    help_box = HelpBox(x=screen_size[0]/2, y=screen_size[1]-100, w=50, h=40, screen_size=(450,300), bc=GREY, text_descr=text_help, original_caption='Pole placement')


    run = True

    while (run):

        pygame.time.delay(100)

        win.fill(BLACK)

        win.blit(text_surface_sys, (screen_size[0]*0.25 - font.size(text_sys)[0]/2, 450))
        win.blit(text_surface_cl, (screen_size[0]*0.75 - font.size(text_cl)[0]/2, 450))

        t_, y_ = cl.step_response(G_cl, t)
        surf = plot(fig, canvas, t_, y_)
        win.blit(surf, fig_pos)

        for slider in sliders:
            slider.draw(win)
        help_box.drawBox(win)

        for event in pygame.event.get():

            if (event.type == pygame.QUIT):
                run = False

            for slider in sliders:
                slider.getEvent(event)

            help_box.getEvent(event, win)

            z_sys = slider_sys_1.val
            p_sys = slider_sys_2.val
            g0_cl = slider_cl_0.val
            z_cl = slider_cl_1.val
            p_cl = slider_cl_2.val

            G = cl.tf(np.poly([z_sys]),np.poly([p_sys]))
            C = g0_cl * cl.tf(np.poly([z_cl]),np.poly([p_cl]))
            G_cl = cl.feedback(C*G,1)

        pygame.display.update()

    pygame.quit()

if __name__ == '__main__':
    main()
