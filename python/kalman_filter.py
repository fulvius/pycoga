import pygame
import numpy as np
import collections
from utility import InputBox, HelpBox

RED = (255, 0, 0)
ORANGE = (255, 153, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
DARK_BLUE = (0, 0, 100)
LIGHT_BLUE = (0, 153, 255)
GREY = (125, 125, 125)
BLACK = (0, 0, 0)

###############################################################################

def kalman(F, H, P, Q, R, x, y):

    # x: state
    # y: measure (mouse pos)

    # prediction
    x_pred = F @ x
    P_pred = F @ P @ F.transpose() + Q

    # update
    S = H @ P_pred @ H.transpose() + R
    K = P_pred @ H.transpose() @ np.linalg.inv(S)
    x_new = x_pred + K @ (y - H @ x_pred)
    P_new = P_pred - K @ H @ P_pred

    return x_new, P_new

###############################################################################

def nPrediction(F, x, n):

    for i in range(n):
        # x = F * x
        x = F @ x

    return x

###############################################################################

def num2pix(x):

    x_p = np.round(x)
    return x_p.astype(int)

###############################################################################

def drawPrediction(x_new, x_npred, limits, win):

    # convert to pixel
    x_new_pix = num2pix(x_new[0:2])
    x_npred_pix = num2pix(x_npred[0:2])
    # clip predicted state
    x_npred_pix[0] = np.clip(x_npred_pix[0], limits[0], limits[2])
    x_npred_pix[1] = np.clip(x_npred_pix[1], limits[1], limits[3])
    pygame.draw.line(win, GREEN, (x_new_pix[0], x_new_pix[1]), (x_npred_pix[0],x_npred_pix[1]))

###############################################################################

def drawLine(x, x_prev, alpha, win, lw=2):

    x1 = num2pix(x)#[0:2]
    x2 = num2pix(x_prev)#[0:2]
    pygame.draw.line(win, (0,0,alpha), x1, x2, lw)

###############################################################################

def drawNoise(x_new, sigma, samples, noise_radius, win):

    noise = np.random.multivariate_normal(x_new[0:2,0], sigma, samples)
    noise = np.round(noise)
    noise = noise.astype(int)
    for i in range(samples):
        pygame.draw.circle(win, ORANGE, (noise[i,0],noise[i,1]), noise_radius, 0)

###############################################################################


def main():

    sigma = np.array([[5,0], [0,5]])
    samples = 5
    dt = 1.0

    F = np.array([[1, 0, dt, 0], [0, 1, 0, dt], [0, 0, 1, 0], [0, 0, 0, 1]])
    H = np.array([[1, 0, 1, 0], [0, 1, 0, 1], [0, 0, 0, 0], [0, 0, 0, 0]])
    Q = np.array([[0.1, 0, 0, 0], [0, 0.1, 0, 0], [0, 0, 0.1, 0], [0, 0, 0, 0.1]])
    R = np.array([[0.1, 0, 0, 0], [0, 0.1, 0, 0], [0, 0, 0.1, 0], [0, 0, 0, 0.1]])
    P = np.array([[10, 0, 0, 0], [0, 10, 0, 0], [0, 0, 10, 0], [0, 0, 0, 10]])

    x = np.zeros((4,1))
    x_pred = np.zeros((4,1))
    y = np.zeros((4,1))

    n = 50 # prediction horizon

    screen_size = [800, 800]
    gap = 10
    limits = (50, 50, screen_size[0]-50, screen_size[1]-200)

    pygame.init()

    win = pygame.display.set_mode((screen_size[0],screen_size[1]))
    pygame.display.set_caption("Kalman filter")

    radius = 3
    thick = 0
    noise_radius = 1

    mouse = mouse_old = (limits[0]/2, limits[1]/2)
    x[0:2,0] = np.asarray(mouse)
    delay = 20

    maxlen = 10
    blue_grad = np.linspace(DARK_BLUE[2], BLUE[2], num=maxlen)
    past_states = collections.deque(maxlen=maxlen)
    for i in range(maxlen):
        past_states.append(x[0:2])

    fading_factor = 3
    fading = 0

    input_box1 = InputBox(x=100, y=screen_size[1]-100, w=50, h=40, text=str(n), text_descr='Horizon prediction', bc=GREY)  # n
    input_box2 = InputBox(x=225, y=screen_size[1]-100, w=50, h=40, text=str(fading_factor), text_descr='Fading', bc=GREY)  # fading
    input_box3 = InputBox(x=325, y=screen_size[1]-100, w=50, h=40, text=str(sigma[1,1]), text_descr='\u03A3', bc=GREY)  # sigma
    input_box4 = InputBox(x=425, y=screen_size[1]-100, w=50, h=40, text=str(Q[1,1]), text_descr='Q', bc=GREY)  # Q
    input_box5 = InputBox(x=525, y=screen_size[1]-100, w=50, h=40, text=str(R[1,1]), text_descr='R', bc=GREY)  # R
    input_boxes = [input_box1, input_box2, input_box3, input_box4, input_box5]

    text_help = "This is a discrete Kalman filter simulation using pygame.\n\nThe blue point represent the estimated cursor position, the orange points"\
                " the noise, the green line the predicted state. the blue fading line the past states.\n\nYou can adjust some parameters:"\
                "\n- Horizon prediction: the number of times the prediction computation occurs.\n- Fading: how much time the fading states remain on the screen."\
                "\n- \u03A3: the diagonal elements of the noise covariance matrix.\n- Q: diagonal elements of input noise covariance matrix."\
                "\n- R: diagonal elements of outputl noise covariance matrix.\n\nClick 'Return' to continue."

    help_box = HelpBox(x=625, y=screen_size[1]-100, w=50, h=40, screen_size=(450,550), bc=GREY, text_descr=text_help, original_caption='Kalman filter')


    run = True
    while (run):
        pygame.time.delay(delay)

        # verify mouse position
        tmp = pygame.mouse.get_pos()
        if (tmp[0] > limits[0] and tmp[0] < limits[2] and tmp[1] > limits[1] and tmp[1] < limits[3]):
            mouse = pygame.mouse.get_pos()

        # estimate mouse velocity
        mouse_vel = (np.asarray(mouse) - np.asarray(mouse_old))/dt
        mouse_old = mouse

        y[0:2,0] = np.asarray(mouse) + np.random.multivariate_normal([0,0], sigma, 1)
        y[2:4,0] = mouse_vel

        # one step kalman filter
        x_new, P_new = kalman(F, H, P, Q, R, x, y)

        # multiple prediction steps
        x_npred = nPrediction(F, x, n)

        # black screen
        win.fill(BLACK)

        # simulation box
        pygame.draw.rect(win, RED, (limits[0]-gap,limits[1]-gap,limits[2]+gap-limits[0],limits[3]+gap-limits[1]), 2)

        # draw prediction
        drawPrediction(x_new, x_npred, limits, win)

        # draw noise
        drawNoise(x_new, sigma, samples, noise_radius, win)

        # draw state
        x_new_pix = num2pix(x_new[0:2])
        pygame.draw.circle(win, BLUE, (x_new_pix[0],x_new_pix[1]), radius, thick)


        # sample past states
        if (fading == fading_factor):
            past_states.append(x_new[0:2])
            fading = 0
        # draw fading past states
        for i in range(len(past_states)-1,0,-1):
            drawLine(past_states[i], past_states[i-1], blue_grad[i], win)

        # update variables
        fading += 1
        x = x_new
        P = P_new

        # events
        for event in pygame.event.get():

            if (event.type == pygame.QUIT):
                run = False

            for box in input_boxes:
                box.getEvent(event)

            help_box.getEvent(event, win)

            # update values
            n = input_box1.value
            fading = fading_factor = input_box2.value
            np.fill_diagonal(sigma, input_box3.value)
            np.fill_diagonal(R, input_box4.value)
            np.fill_diagonal(Q, input_box5.value)

        # update boxes
        for box in input_boxes:
            box.draw(win)

        help_box.drawBox(win)

        # update screen
        pygame.display.update()


    pygame.quit()


if __name__ == '__main__':
    main()
