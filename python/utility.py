import pygame
import numpy as np

class InputBox():
    """docstring for InputBox."""
    def __init__(self, x, y, w, h, text='', text_descr='', bc=(0,0,0), fc=(255,0,0)):

        self.rect = pygame.Rect(x, y, w, h)
        self.bc = bc
        self.fc = fc
        self.text = text
        self.text_descr = text_descr
        self.font = pygame.font.Font('freesansbold.ttf', 16)
        self.font_descr = pygame.font.Font('freesansbold.ttf', 14)
        self.text_surface = self.font.render(self.text, True, self.fc)
        self.text_descr_surface = self.font_descr.render(self.text_descr, True, (255,255,255))
        self.getValue()

    def getValue(self):
        a = self.text.find('.')
        if (a==-1):
            self.value = int(self.text)
        else:
            self.value = float(self.text)

    def getEvent(self, event):

        if (event.type == pygame.MOUSEBUTTONUP):
            tmp = pygame.mouse.get_pos()

            if (tmp[0]>self.rect.x and tmp[0]<self.rect.x+self.rect.w and tmp[1]>self.rect.y and tmp[1]<self.rect.y+self.rect.h):
                print("Clicked '%s' box. Insert number. Click 'Return' to complete." % self.text_descr)

            if self.rect.collidepoint(event.pos):
                self.text = self.getData(self.text)
                self.getValue()
                self.text_surface = self.font.render(self.text, True, self.fc)

    def draw(self, win):
        pygame.draw.rect(win, self.bc, self.rect, 2)
        win.blit(self.text_surface, (self.rect.x + self.rect.w/2 - self.font.size(self.text)[0]/2, self.rect.y + self.rect.h/2 - self.font.size(self.text)[1]/2 ))
        win.blit(self.text_descr_surface, (self.rect.x + self.rect.w/2 - self.font_descr.size(self.text_descr)[0]/2 , self.rect.y - 20))

    def getData(self, destiny):

        print(destiny)

        run = True
        while (run):

            for event in pygame.event.get():

                # event = pygame.event.wait()
                keys = pygame.key.get_pressed()

                if (keys[pygame.K_RETURN]):
                    print("Break!")
                    run = False
                if (keys[pygame.K_0]):
                    destiny += str(chr(pygame.K_0))
                    print(destiny)
                if (keys[pygame.K_1]):
                    destiny += str(chr(pygame.K_1))
                    print(destiny)
                if (keys[pygame.K_2]):
                    destiny += str(chr(pygame.K_2))
                    print(destiny)
                if (keys[pygame.K_3]):
                    destiny += str(chr(pygame.K_3))
                    print(destiny)
                if (keys[pygame.K_4]):
                    destiny += str(chr(pygame.K_4))
                    print(destiny)
                if (keys[pygame.K_5]):
                    destiny += str(chr(pygame.K_5))
                    print(destiny)
                if (keys[pygame.K_6]):
                    destiny += str(chr(pygame.K_6))
                    print(destiny)
                if (keys[pygame.K_7]):
                    destiny += str(chr(pygame.K_7))
                    print(destiny)
                if (keys[pygame.K_8]):
                    destiny += str(chr(pygame.K_8))
                    print(destiny)
                if (keys[pygame.K_9]):
                    destiny += str(chr(pygame.K_9))
                    print(destiny)
                if (keys[pygame.K_COMMA]):
                    destiny += "."
                    print(destiny)
                if (keys[pygame.K_BACKSPACE]):
                    destiny = destiny[:-1]
                    print(destiny)
        return destiny

###############################################################################
###############################################################################
###############################################################################

class HelpBox():
    """docstring for HelpBox."""
    def __init__(self, x, y, w, h, screen_size=(400,400), text="?", text_descr="", fc=(255,0,0), bc=(0,0,0), ft=16, original_caption=""):

        self.rect = pygame.Rect(x, y, w, h)
        self.text = text
        self.text_descr = text_descr
        self.fc = fc
        self.fc2 = (255,255,255)
        self.bc = bc
        self.bc2 = (0,0,0)
        self.ft = ft
        self.ft2 = 16
        self.font = pygame.font.Font('freesansbold.ttf', self.ft)
        self.font2 = pygame.font.Font('freesansbold.ttf', self.ft2)
        self.text_surface = self.font.render(self.text, True, self.fc)
        self.original_caption = original_caption
        self.screen_size = screen_size

    def getEvent(self, event, win):

        if (event.type == pygame.MOUSEBUTTONUP):

            tmp = pygame.mouse.get_pos()

            if (tmp[0]>self.rect.x and tmp[0]<self.rect.x+self.rect.w and tmp[1]>self.rect.y and tmp[1]<self.rect.y+self.rect.h):
                self.drawHelpBox(win)

    def drawBox(self, win):
        pygame.draw.rect(win, self.bc, self.rect, 2)
        win.blit(self.text_surface, (self.rect.x + self.rect.w/2 - self.font.size(self.text)[0]/2, self.rect.y + self.rect.h/2 - self.font.size(self.text)[1]/2 ))

    def drawHelpBox(self, win):

        original_size = win.get_size()

        win = pygame.display.set_mode(self.screen_size)
        pygame.display.set_caption("README")
        init_pos = (10,10)

        font = pygame.font.Font('freesansbold.ttf', 18)

        run = True
        while (run):

            self.blitText(win, self.text_descr, init_pos, font)

            for event in pygame.event.get():

                keys = pygame.key.get_pressed()
                if (keys[pygame.K_RETURN]):
                    run = False

            pygame.display.update()

        win = pygame.display.set_mode(original_size)
        pygame.display.set_caption(self.original_caption)



    def blitText(self, win, text, init_pos, font):

        words = [word.split(' ') for word in text.splitlines()]
        space = font.size(' ')[0]
        max_width, max_height = win.get_size()
        x, y = init_pos
        for line in words:
            for word in line:
                word_surface = font.render(word, 0, (255,255,255))
                word_width, word_height = word_surface.get_size()
                if x + word_width >= max_width:
                    x = init_pos[0]
                    y += word_height
                win.blit(word_surface, (x,y))
                x += word_width + space
            x = init_pos[0]
            y += word_height

###############################################################################
###############################################################################
###############################################################################

class Slider():
    """docstring for Slider."""
    def __init__(self, x, y, w, h, ws, hs, val, min_val, max_val, text="", ft=16, fc=(0,0,0)):

        self.rect = pygame.Rect(x, y, w, h)
        self.rect_slide = pygame.Rect(x-ws/2, y+h/2-hs/2, ws, hs)
        self.rect_val = pygame.Rect(x+w+25, y-20, 60, 40)
        self.min_val = min_val
        self.max_val = max_val
        self.min_x = x - ws/2
        self.max_x = x + w + ws/2
        self.fc1 = (0,0,255)
        self.bc1 = (0,0,255)
        self.fc2 = (0,255,0)
        self.bc2 = (0,255,0)
        self.val = val
        self.inverseMapping()
        self.ft = ft
        self.fc = fc
        self.text = text
        self.text_val = "%.2f" % self.val
        self.font = pygame.font.Font('freesansbold.ttf', self.ft)
        self.text_surface = self.font.render(self.text, True, self.fc)
        self.text_surface_val = self.font.render(self.text_val, True, self.fc)

    def draw(self, win):

        pygame.draw.rect(win, self.bc1, self.rect, 2)
        pygame.draw.rect(win, self.bc2, self.rect_slide, 1)
        win.blit(self.text_surface, (self.rect.x + self.rect.w/2 - self.font.size(self.text)[0]/2 , self.rect_slide.y - self.font.size(self.text)[1]-10))
        pygame.draw.rect(win, (125,125,125), self.rect_val, 2)
        self.text_val = "%.2f" % self.val
        self.text_surface_val = self.font.render(self.text_val, True, self.fc)
        win.blit(self.text_surface_val, (self.rect_val.x + self.rect_val.w/2 - self.font.size(self.text_val)[0]/2, self.rect_val.y + self.rect_val.h/2 - self.font.size(self.text_val)[1]/2 ))

    def mapping(self):
        # self.val = self.min_val + (self.rect_slide.x - self.rect.x)*(self.max_val-self.min_val)/(self.rect.w - self.rect_slide.w/2)
        self.val = self.min_val + (self.rect_slide.x - self.rect.x)*(self.max_val-self.min_val)/(self.rect.w - self.rect_slide.w)


    def inverseMapping(self):
        self.rect_slide.x = self.min_x + (self.val - self.min_val)*(self.max_x - self.min_x)/(self.max_val - self.min_val)

    def getEvent(self, event):

        if (pygame.mouse.get_pressed()[0]):
            tmp = pygame.mouse.get_pos()
            # if (tmp[0]>self.rect.x and tmp[0]<self.rect.x+self.rect.w and tmp[1]>self.rect.y and tmp[1]<self.rect.y+self.rect.h):
                # self.rect_slide.x = np.clip(tmp[0] - self.rect_slide.w/2, self.rect.x, self.rect.x+self.rect.w)
            if (tmp[0]>self.rect.x - self.rect_slide.w/2 and tmp[0]<self.rect.x+self.rect.w+self.rect_slide.w/2 and tmp[1]>self.rect.y and tmp[1]<self.rect.y+self.rect.h):
                self.rect_slide.x = np.clip(tmp[0] , self.rect.x, self.rect.x+self.rect.w-self.rect_slide.w)
                self.mapping()
