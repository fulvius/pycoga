# PYCOGA

PYCOGA (PYthon COntrol GAmes) is a set of games made for fun with focus on control theory.

## List of implemented games

- Kalman filter
- Pole placement
- Cart pole
- Reach target
- Push target
- Neural network training

## Environments

- Python
